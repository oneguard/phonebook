<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="java.sql.Connection,java.sql.DriverManager" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.Statement" %>
<link rel="stylesheet" href="bootstrap-4.0.0-dist/css/bootstrap.min.css">
<form METHOD="post" action="se.jsp">
    <div class="form-group">
        <h3><label>Поиск контакта</label></h3>
        <input name="stringSearch" type="login" class="form-control" placeholder="Введите контакт">
    </div>
    <button type="submit" class="btn btn-primary">Найти</button>
</form>
<table class="table table-striped">
    <thead>
    <tr>
    <tr class="table-danger">
        <th scope="col">#</th>
        <th scope="col">Фамилия</th>
        <th scope="col">Имя</th>
        <th scope="col">Отчество</th>
        <th scope="col">Телефон</th>
        <th scope="col"></th>
        <th scope="col"></th>
    </tr>
    </thead>
    <tbody>
        <%

      String dbDriver = "com.mysql.jdbc.Driver";
    String dbURL = "jdbc:mysql://localhost:3306/";
    String dbName = "phonebook";
    String publicKey = "?allowPublicKeyRetrieval=true";
    String ssh = "&useSSL=false";
    String jdbcCompliantTruncation = "&jdbcCompliantTruncation=false";
    String dbUsername = "root";
    String dbPassword = "root";

try{
    Class.forName(dbDriver);
    Connection connect = DriverManager.getConnection(dbURL + dbName + publicKey + ssh + jdbcCompliantTruncation, dbUsername, dbPassword);
    ResultSet rs;
    Statement st = connect.createStatement();
    String createTableSql = "CREATE TABLE book (" +
            "id int AUTO_INCREMENT NOT NULL, " +
            "Userfamily VARCHAR(50) NOT NULL," +
            "Username VARCHAR(50) NOT NULL," +
            "Userpatronymic VARCHAR(50) NOT NULL," +
            "Usernumber VARCHAR(20) NOT NULL," +
            "PRIMARY KEY (id))";
    try {
        st.executeUpdate(createTableSql);
    } catch (Exception e) { }
        String sqlQuery = "SELECT * FROM book";
        rs = st.executeQuery(sqlQuery);
        while(rs.next()){%>
    <tr>
    <tr class="table-warning">
        <th scope="row"><%=rs.getInt("id")%>
        </th>
        <td><%=rs.getString("Userfamily")%>
        </td>
        <td><%=rs.getString("Username")%>
        </td>
        <td><%=rs.getString("Userpatronymic")%>
        </td>
        <td><%=rs.getString("Usernumber")%>
        </td>
        <td><a href="update.jsp?id=<%=rs.getInt("id")%>">
            <button type="button" class="update">Изменить</button>
        </a></td>
        <td><a href="delete.jsp?id=<%=rs.getInt("id")%>">
            <button type="button" class="delete">Удалить</button>
        </a></td>
    </tr>
        <%
}
rs.close();
st.close();
connect.close();
}
catch(Exception e){
	out.println(e);
	e.printStackTrace();	
}
%>

</table>

<div>
    <button onclick="location.href='add'">Новый контакт</button>
    <button onclick="location.href='index.jsp'">Главная страница</button>
    <br><br>
</div>