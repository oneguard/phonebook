<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page import="java.sql.PreparedStatement" %>
<%@ page import="java.sql.SQLException" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%
    String id = request.getParameter("id");
    String family = request.getParameter("Userfamily");
    String name = request.getParameter("Username");
    String patronymic = request.getParameter("Userpatronymic");
    String number = request.getParameter("Usernumber");
    String dbDriver = "com.mysql.jdbc.Driver";
    String dbURL = "jdbc:mysql://localhost:3306/";
    String dbName = "phonebook";
    String publicKey = "?allowPublicKeyRetrieval=true";
    String ssh = "&useSSL=false";
    String dbUsername = "root";
    String dbPassword = "root";

    if (id != null) {
        Connection con;
        PreparedStatement ps;
        int contactId = Integer.parseInt(id);
        try {
            try {
                Class.forName(dbDriver);
            } catch (ClassNotFoundException e) {
                request.setAttribute("error", e);
                out.println(e);
            }
            con = DriverManager.getConnection(dbURL + dbName + publicKey + ssh, dbUsername, dbPassword);
            String sql = "Update book set Userfamily=?,Username=  ?,Userpatronymic=  ?,Usernumber=  ? where id=" + contactId;
            ps = con.prepareStatement(sql);
            ps.setString(1, family);
            ps.setString(2, name);
            ps.setString(3, patronymic);
            ps.setString(4, number);

            int i = ps.executeUpdate();
            if (i > 0) {
                out.print("Контакт изменён");
            } else {
                out.print("Изменение не выполнено.");
            }
        } catch (SQLException sql) {
            request.setAttribute("error", sql);
            out.println(sql);
        }
    }
%>

<br><br>
<div>
    <button onclick="location.href='list'">Вернуться к списку контактов</button>
</div>