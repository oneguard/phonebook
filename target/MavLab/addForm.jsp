<!DOCTYPE html>
<html>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<head>
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
</head>

<body>
<form method="post" action="addContact.jsp">
    <h1>Добавление контакта</h1>
    <label>Фамилия:   <input type="text" name="Userfamily"></label>
    <br><br>
    <label>Имя:   <input type="text" name="Username"></label>
    <br><br>
    <label>Отчество:   <input type="text" name="Userpatronymic"></label>
    <br><br>
    <label>Телефон:   <input type="text" name="Usernumber"></label>
    <br><br>
    <input type="Submit" value="Добавить контакт">
    <input type="Button" value="Отмена" onclick="location.href='index.jsp'">

    <script>
        $(function () {
            $.datepicker.setDefaults({
                onClose: function (date) {
                    $("#selectedDateVal").html(date);
                }
            });

            $("#datepicker").datepicker();
        });
    </script>
</form>
</body>
</html>